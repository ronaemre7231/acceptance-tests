/* globals gauge*/
"use strict";
const path = require('path');
const {
    $,
    focus,
    openBrowser,
    write,
    closeBrowser,
    goto,
    press,
    screenshot,
    above,
    click,
    checkBox,
    listItem,
    toLeftOf,
    link,
    text,
    into,
    textBox,
    evaluate
} = require('taiko');
const assert = require("assert");
const headless = process.env.headless_chrome.toLowerCase() === 'true';

beforeSuite(async () => {
    await openBrowser({
        headless: headless
    })
});

afterSuite(async () => {
    await closeBrowser();
});

// Return a screenshot file name
gauge.customScreenshotWriter = async function () {
    const screenshotFilePath = path.join(process.env['gauge_screenshots_dir'],
        `screenshot-${process.hrtime.bigint()}.png`);

    await screenshot({
        path: screenshotFilePath
    });
    return path.basename(screenshotFilePath);
};

step("When I am on my Todo page", async () => {

    await goto("https://todocase-rc4lv2g7pq-uc.a.run.app");

});

step('I should see navbar header name', async () => {
    assert.ok(await $('#navbar_header').exists());
});

step('I should see Add Todo components inputs', async () => {
    assert.ok(await $('#todo-name').exists());
    assert.ok(await $('#todo-details').exists());
});

step('I should see Add Todo Button', async () => {
    assert.ok(await text('Add Todo').exists());
});

step('After the fill inputs correctly and When I am click Add Todo button ı should see Succesfully message on screen', async () => {
    await click($('#todo-name'))
    await write("deneme todo name")
    await click($('#todo-details'))
    await write("deneme todo details")
    await click(text('Add Todo'))
    assert.ok(await $('#saved-succesfuly').exists());
    await goto("http://localhost:3001");
});


step('If there is any todo ı should see name and details of todo', async () => {
    await click($('#todo-name'))
    await write("deneme todo name2")
    await click($('#todo-details'))
    await write("deneme todo details2")
    await click(text('Add Todo'))
    await goto("http://localhost:3001");
    assert.ok(await $('#todo-info-name').exists());
    assert.ok(await $('#todo-info-details').exists());
});



